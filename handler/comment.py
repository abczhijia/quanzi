# coding:utf8

from .base import BaseHandler
from tornado.gen import coroutine
from util import auth


class ApiCommentAddHandler(BaseHandler):
    @auth.api_login_required
    @coroutine
    def post(self, *args, **kwargs):
        user_id = self.get_user_id()
        article_id = self.get_argument('article_id')
        content = self.get_argument('content', '').strip()
        data = yield self.comment_model.create(content, user_id, article_id)
        self.write_json_ok(data)


class ApiCommentListHandler(BaseHandler):
    @coroutine
    def get(self, *args, **kwargs):
        article_id = int(self.get_argument('article_id', 0))
        page = int(self.get_argument('page', 1))
        data = yield self.comment_srv.find_list_by_article_id(article_id, page)
        self.write_json_ok(data)


class ApiCommentDetailHandler(BaseHandler):
    @coroutine
    def get(self, id):
        data = yield self.comment_model.find_by_id(id)
        self.write_json_ok(data)
