# coding:utf8

from .base import BaseHandler
from tornado.gen import coroutine
from util import auth


class ApiArticleAddHandler(BaseHandler):
    @auth.api_login_required
    @coroutine
    def post(self, *args, **kwargs):
        user_id = self.get_user_id()
        content = self.get_argument('content', '').strip()
        if len(content) < 5:
            self.write_json_err(self.error.article_content_length_err)
            return

        image = self.get_argument('image', '').strip()
        data = yield self.article_model.create(content, image, user_id)
        self.write_json_ok(data)


class ApiArticleListHandler(BaseHandler):
    @coroutine
    def get(self, *args, **kwargs):
        page = int(self.get_argument('page', 1))
        user_id = int(self.get_argument('user_id', 0))
        data = yield self.article_model.find_list(user_id=user_id, page=page)
        self.write_json_ok(data)


class ApiArticleDetailHandler(BaseHandler):
    @coroutine
    def get(self, id):
        data = yield self.article_srv.find_by_id(id)
        self.write_json_ok(data)
