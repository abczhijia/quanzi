# coding:utf8

from .base import BaseHandler
from tornado.gen import coroutine
from util import auth


class ApiLikeAddHandler(BaseHandler):
    @auth.api_login_required
    @coroutine
    def post(self, *args, **kwargs):
        user_id = self.get_user_id()
        article_id = self.get_argument('article_id')
        content = 1 if int(self.get_argument('content', 1).strip()) == 1 else 0

        data = yield self.like_model.is_like_exist(article_id, user_id)
        print data
        if data is not None:
            self.write_json_err(self.error.like_repeat_err)
            return

        data = yield self.like_model.create(content, article_id, user_id)
        self.write_json_ok(data)
