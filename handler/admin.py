# coding:utf8
from .base import BaseHandler
from tornado.gen import coroutine


class IndexHandler(BaseHandler):
    @coroutine
    def get(self):
        courses = yield self.course_model.find_list_by_parent_id(0)
        self.render('admin/index.html', active='dashboard', courses=courses)


class UserListHandler(BaseHandler):
    @coroutine
    def get(self):
        page = int(self.get_argument('page', 1))
        users, page_info = yield self.user_model.find_list_by_page(page)
        print users, page_info
        self.render('admin/user-list.html', active='user-list', users=users, page_info=page_info)


class UserAddHandler(BaseHandler):
    @coroutine
    def get(self):
        self.render('admin/user-add.html', active='user-add')


class CourseListHandler(BaseHandler):
    @coroutine
    def get(self):
        self.render('admin/course-list.html', active='course-list')


class CourseAddHandler(BaseHandler):
    @coroutine
    def get(self):
        self.render('admin/course-add.html', active='course-add')
