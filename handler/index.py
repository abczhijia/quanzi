# coding:utf8
from .base import BaseHandler
from tornado.gen import coroutine
from util import auth


class IndexHandler(BaseHandler):
    @auth.page_login_required
    @coroutine
    def get(self):
        data = yield self.user_model.find_list(0)
        self.render('index.html', data=data)
