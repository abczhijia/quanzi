# coding:utf8
from .base import BaseHandler
from tornado.gen import coroutine
from util import auth
import os.path
import time

upload_path = os.path.join(os.path.dirname(os.path.dirname(os.path.abspath(__file__))), 'front', 'static', 'upload',
                           'image')


class UploadImageHandler(BaseHandler):
    def get(self, *args, **kwargs):
        self.render('upload-image.html')


class ApiUploadImageHandler(BaseHandler):
    @coroutine
    def post(self):
        size = float(self.request.headers.get('content-length')) / (1024 * 1024)
        if size > 2:
            self.write_json_err(self.error.upload_image_size_err)
            return

        file_metas = self.request.files['file']
        if len(file_metas) == 0:
            self.write_json_err(self.error.upload_image_zero_err)
            return

        if len(file_metas) > 1:
            self.write_json_err(self.error.upload_image_count_err)
            return

        meta = file_metas[0]
        content_type = meta['content_type']
        if content_type not in ['image/png', 'image/jpeg', 'image/gif']:
            self.write_json_err(self.error.upload_image_format_err)
            return

        filename = '%s-%s-%s' % (self.get_user_id(), int(time.time() * 1000), meta['filename'][0:30])
        file_path = os.path.join(upload_path, filename)
        with open(file_path, 'wb') as f:
            f.write(meta['body'])

        self.write_json_ok(filename)
