# coding:utf8
from tornado.web import RequestHandler
from tornado.gen import coroutine, Return
import json
from datetime import datetime, date
from util import error
from model.user import user_model
from model.article import article_model
from model.comment import comment_model
from model.like import like_model

from service.article import article_srv
from service.user import user_srv
from service.comment import comment_srv


class BaseHandler(RequestHandler):
    def __init__(self, *args, **kwargs):
        super(BaseHandler, self).__init__(*args, **kwargs)
        self.error = error
        self.user_model = user_model
        self.article_model = article_model
        self.comment_model = comment_model
        self.like_model = like_model

        self.user_srv = user_srv
        self.article_srv = article_srv
        self.comment_srv = comment_srv

    def write_json_ok(self, data=''):
        callback = self.get_argument('callback', False)
        if callback:
            self.set_header('content-type', 'text/javascript')
            data = {'err': 0, 'data': self.__json(data)}
            self.write("""try{%s(%s)}catch(err){}""" % (callback, self.__dumps(data)))
        else:
            self.write({'err': 0, 'data': self.__json(data)})
        self.finish()

    def write_json_err(self, err):
        self.write({'err': err.err, 'msg': err.msg})
        self.finish()

    def __json(self, data):
        return json.loads(self.__dumps(data))

    def __dumps(self, data):
        return json.dumps(data, cls=DatetimeJsonEncoder)

    @coroutine
    def get_user_info(self):
        if self.user_info:
            raise Return(self.user_info)
        self.user_info = yield self.user_model.find_by_token(self.get_token())
        raise Return(self.user_info)

    def get_token(self):
        try:
            token = self.request.headers['token']  # or self.get_secure_cookie('token')
        except KeyError as e:
            token = None
        return token

    def get_user_id(self):
        try:
            user_id = self.get_secure_cookie('user_id') or self.request.headers['user_id']
            return int(user_id)
        except KeyError as e:
            return None


class DatetimeJsonEncoder(json.JSONEncoder):
    def default(self, o):
        if isinstance(o, datetime):
            return o.strftime('%Y-%m-%d %H:%M:%S')
        elif isinstance(o, date):
            return o.strftime('%Y-%m-%d')
        else:
            return json.JSONEncoder.default(self, o)
