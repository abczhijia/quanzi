# coding:utf8
from .base import BaseHandler
from tornado.gen import coroutine
from util.auth import page_login_required, api_login_required


class UserLoginHandler(BaseHandler):
    @coroutine
    def get(self):
        self.render('login.html')


class UserRegisterHandler(BaseHandler):
    @page_login_required
    @coroutine
    def get(self):
        self.render('register.html')


class ApiUserListHandler(BaseHandler):
    @api_login_required
    @coroutine
    def get(self):
        last_id = int(self.get_argument('last_id', 0))
        data = yield self.user_model.find_list(last_id)
        self.write_json_ok(data)


class ApiUserDetailHandler(BaseHandler):
    @coroutine
    def get(self, id):
        data = yield self.user_model.find_by_id(id)
        print self.get_secure_cookie('token')
        self.write_json_ok(data)


class ApiUserRegisterHandler(BaseHandler):
    @coroutine
    def post(self, *args, **kwargs):
        phone = self.get_argument('phone')
        password = self.get_argument('password')

        data = yield self.user_model.is_phone_exists(phone)
        if data is not None:
            self.write_json_err(self.error.user_phone_exists)
            return

        data = yield self.user_model.create(phone, password)
        self.write_json_ok(data)


class ApiUserLoginHandler(BaseHandler):
    @coroutine
    def post(self, *args, **kwargs):
        phone = self.get_argument('phone')
        password = self.get_argument('password')
        data = yield self.user_srv.login_by_phone_password(phone, password)
        if data is None:
            self.write_json_err(self.error.user_login_phone_password_err)
        else:
            self.set_secure_cookie('token', data.token)
            self.set_secure_cookie('user_id', str(data.id))
            self.set_secure_cookie('username', data.username)
            self.set_secure_cookie('phone', data.phone)
            self.write_json_ok(data)


class ApiUserLogoutHandler(BaseHandler):
    @api_login_required
    @coroutine
    def post(self, *args, **kwargs):
        token = self.get_token()
        data = yield self.user_model.update_token(token)
        print 'update: ', data
        self.clear_cookie('token')
        self.clear_cookie('user_id')
        self.clear_cookie('username')
        self.clear_cookie('phone')
        self.write_json_ok()
