DROP DATABASE IF EXISTS quanzi;
CREATE DATABASE quanzi
  DEFAULT CHARACTER SET utf8
  COLLATE utf8_general_ci;
USE quanzi;

# 用户表
CREATE TABLE `user` (
  `id`         INT(11) UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT,
  `phone`      VARCHAR(64)      NOT NULL UNIQUE,
  `username`   VARCHAR(64)      NOT NULL UNIQUE,
  `password`   VARCHAR(32)      NOT NULL,
  `status`     TINYINT          NOT NULL, #用户状态：1为可用，0为不可用
  `token`      VARCHAR(40)      NOT NULL,
  `created_at` DATETIME         NOT NULL,
  `updated_at` DATETIME         NOT NULL
)
  ENGINE = innodb
  AUTO_INCREMENT = 1
  DEFAULT CHARSET = utf8;

#动态表
CREATE TABLE `article` (
  `id`         INT(10) UNSIGNED NOT NULL PRIMARY KEY      AUTO_INCREMENT,
  `content`    VARCHAR(64)      NOT NULL,
  `image`      VARCHAR(255)     NOT NULL,
  `status`     TINYINT          NOT NULL, #1可用，0，禁用
  `created_at` DATETIME         NOT NULL,
  `updated_at` DATETIME         NOT NULL,
  `user_id`    INT              NOT NULL
)
  ENGINE = innodb
  AUTO_INCREMENT = 1
  DEFAULT CHARSET = utf8;

#评论表
CREATE TABLE `comment` (
  `id`         INT(10) UNSIGNED NOT NULL PRIMARY KEY      AUTO_INCREMENT,
  `content`    VARCHAR(64)      NOT NULL,
  `status`     TINYINT          NOT NULL, #1：可用，0：禁用, 2：已删除
  `created_at` DATETIME         NOT NULL,
  `updated_at` DATETIME         NOT NULL,
  `article_id` INT              NOT NULL,
  `user_id`    INT              NOT NULL
)
  ENGINE = innodb
  AUTO_INCREMENT = 1
  DEFAULT CHARSET = utf8;

#点赞踩表
CREATE TABLE `like` (
  `id`         INT(10) UNSIGNED NOT NULL PRIMARY KEY      AUTO_INCREMENT,
  `content`    TINYINT          NOT NULL, #1赞， 2： 踩
  `status`     TINYINT          NOT NULL, #1已赞，0，已取消
  `created_at` DATETIME         NOT NULL,
  `updated_at`  DATETIME         NOT NULL,
  `article_id` INT              NOT NULL,
  `user_id`    INT              NOT NULL
)
  ENGINE = innodb
  AUTO_INCREMENT = 1
  DEFAULT CHARSET = utf8;

