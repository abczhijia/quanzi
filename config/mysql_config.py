# coding:utf8
from tornado.options import options

__development_config = dict(
    host='localhost',
    port=3306,
    user='root',
    password='Woshi123',
    database='quanzi',
    init_command='set names utf8'
)

__production_config = dict(
    host='localhost',
    port=3306,
    user='root',
    password='newpass',
    database='quanzi',
    init_command='set names utf8'
)

config = __development_config if options.dev else __production_config
