# coding:utf8

class Error(object):
    def __init__(self, err, msg):
        self.err = err
        self.msg = msg


# user
user_username_exists = Error(1000, '用户名已存在')
user_phone_exists = Error(1001, '邮箱已存在')
user_login_no = Error(1002, '还未登录哦，请先登录')
user_login_username_password_err = Error(1003, '用户名或密码错误')
user_login_phone_password_err = Error(1004, '邮箱或密码错误')
user_token_expired = Error(1005, 'token已失效啦，请重新登录')

# article
article_content_length_err = Error(1100, '动态内容至少5个字哦')

# comment
comment_content_length_err = Error(1100, '评论内容至少5个字哦')

# upload
upload_image_size_err = Error(1200, '上传图片不能大于2M哦')
upload_image_count_err = Error(1201, '一次只能上传一张图片哦')
upload_image_zero_err = Error(1202, '上传图片数不能为0哦')
upload_image_format_err = Error(1202, '上传图片格式不对')

# like
like_repeat_err = Error(1300, '你已经点过赞啦')
