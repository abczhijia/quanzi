# coding:utf8
from functools import wraps
from tornado.gen import coroutine, Return


def page_login_required(func):
    @wraps(func)
    @coroutine
    def decorated_func(handler, *args, **kwargs):
        if handler.get_secure_cookie('token') is None:
            handler.redirect('/user/login?next=%s' % handler.request.path)
            return
        yield func(handler, *args, **kwargs)

    return decorated_func


def api_login_required(func):
    @wraps(func)
    @coroutine
    def decorated_func(handler, *args, **kwargs):
        token = handler.get_token()
        # 先校验是否有cookie或者头部token，如果没有，直接返回err
        if token is None:
            handler.write_json_err(handler.error.user_login_no)
            return
        # 查询token是否有效，如果无效，也返回err
        data = yield handler.user_model.find_by_token(token)
        if data is None:
            handler.write_json_err(handler.error.user_token_expired)
            return
        yield func(handler, *args, **kwargs)

    return decorated_func
