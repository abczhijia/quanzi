# coding:utf8
from base import BaseModel
from tornado.gen import coroutine, Return


class __ArticleModel(BaseModel):
    @coroutine
    def find_by_id(self, id):
        sql = '''select *, 
                    (select count(id) from `like` where `like`.article_id=article.id) as like_count,
                    (select count(id) from `comment` where `comment`.article_id=article.id) as comment_count
              from article where id=%s''' % id
        data = yield self.mysql.get(sql)
        raise Return(data)

    @coroutine
    def find_list(self, user_id=0, page=1):
        count = self.count
        print user_id
        if user_id == 0:
            sql = '''select *,
                    (select count(id) from `like` where `like`.article_id=article.id) as like_count,
                    (select count(id) from `comment` where `comment`.article_id=article.id) as comment_count
                  from article where status=1 limit %s, %s'''
            data = yield self.mysql.query(sql, count * (page - 1), count)
        else:
            sql = '''select *, 
                    (select count(id) from `like` where `like`.article_id=article.id) as like_count,
                    (select count(id) from `comment` where `comment`.article_id=article.id) as comment_count
                  from article where user_id=%s limit %s, %s'''
            data = yield self.mysql.query(sql, user_id, count * (page - 1), count)
        raise Return(data)

    @coroutine
    def create(self, content, image, user_id):
        sql = 'insert into article value (NULL , %s, %s, %s, %s, %s, %s)'
        now = self.helper.now()
        created_at = now
        updated_at = now
        status = 1
        # 题目
        data = yield self.mysql.execute(sql, content, image, status, created_at, updated_at, user_id)
        raise Return(data)

    @coroutine
    def update(self, id, name, detail):
        sql = 'update article as t set t.name=%s, t.detail=%s, t.updated_at=%s where id=%s'
        now = self.helper.now()
        updated_at = now
        data = yield self.mysql.execute(sql, name, detail, updated_at, id)
        raise Return(data)


article_model = __ArticleModel()
