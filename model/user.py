# coding:utf8
from base import BaseModel
from tornado.gen import coroutine, Return
import string, hashlib, random


class __UserModel(BaseModel):
    '''UserModel'''

    def _generate_password(self, password):
        salt = '9a2ba12e92171148c1cf7d06158b3e498259275a'
        return hashlib.md5(password + salt).hexdigest()

    def _generate_token(self):
        return hashlib.sha1(''.join(random.sample(string.printable, 64))).hexdigest()

    @coroutine
    def find_by_id(self, id):
        sql = 'select id, phone, username, status, token, created_at, updated_at from user where id=%s'
        data = yield self.mysql.get(sql, id)
        raise Return(data)

    @coroutine
    def find_by_token(self, token):
        sql = 'select * from user where token=%s'
        data = yield self.mysql.get(sql, token)
        raise Return(data)

    @coroutine
    def find_list(self, last_id=0, count=0):
        sql = 'select * from user '
        data = yield self.mysql.query(sql)
        raise Return(data)

    @coroutine
    def find_list_by_page(self, page=1):
        count = self.count
        start = (page - 1) * count
        end = start + count
        sql = 'select * from user limit %s, %s'
        users, info = yield [self.mysql.query(sql, start, end),
                             self.mysql.get('select count(id) as count from user')]
        page_info = {"total": int(round(info.count * 1.0 / count)), 'count': count, 'page': page}
        raise Return((users, page_info))

    @coroutine
    def find_by_username_password(self, username, password):
        sql = 'select id, phone, username, status, token, created_at, updated_at from user where username=%s and password=%s limit 1'
        data = yield self.mysql.get(sql, username, self._generate_password(password))
        raise Return(data)

    @coroutine
    def find_by_phone_password(self, phone, password):
        sql = 'select id, phone, username, status, token, created_at, updated_at from user where phone=%s and password=%s limit 1'
        data = yield self.mysql.get(sql, phone, self._generate_password(password))
        raise Return(data)

    @coroutine
    def is_username_exists(self, username):
        sql = 'select id from user where username=%s limit 1'
        data = yield self.mysql.get(sql, username)
        raise Return(data)

    @coroutine
    def is_phone_exists(self, phone):
        sql = 'select * from user where phone=%s limit 1'
        data = yield self.mysql.get(sql, phone)
        raise Return(data)

    @coroutine
    def create(self, phone, password):
        sql = 'insert into user value (NULL , %s, %s, %s, %s, %s, %s, %s)'
        password = self._generate_password(password)
        status = 1
        token = self._generate_token()
        now = self.helper.now()
        username = phone
        created_at = now
        updated_at = now
        data = yield self.mysql.execute(sql, phone, username, password, status, token, created_at, updated_at)
        raise Return(data)

    @coroutine
    def update_token(self, token):
        '''这里之所以要传入token，而不是uid，是因为token是私密的，而uid是公开的'''
        sql = 'update user set token=%s where token=%s'
        new_token = self._generate_token()
        data = yield self.mysql.execute(sql, new_token, token)
        raise Return(data)

    @coroutine
    def update_password(self, username, password):
        sql = 'update user set password=%s where username=%s and password=%s'
        new_password = self._generate_password()
        data = yield self.mysql.execute(sql, new_password)
        raise Return(data)


user_model = __UserModel()
