# coding:utf8
from base import BaseModel
from tornado.gen import coroutine, Return


class __LikeModel(BaseModel):
    @coroutine
    def find_by_id(self, id):
        sql = 'select * from `like` where id=%s' % id
        data = yield self.mysql.get(sql)
        raise Return(data)

    @coroutine
    def find_list(self, course_id, parent_id=0):
        sql = 'select * from `like` where parent_id=%s and course_id=%s'
        data = yield self.mysql.query(sql, parent_id, course_id)
        raise Return(data)

    @coroutine
    def find_count_by_article_id(self):
        pass

    @coroutine
    def create(self, content, article_id, user_id):
        sql = 'insert into `like` value (NULL , %s, %s, %s, %s, %s, %s)'
        now = self.helper.now()
        created_at = now
        updated_at = now
        status = 1
        data = yield self.mysql.execute(sql, content, status, created_at, updated_at, article_id, user_id)
        raise Return(data)

    @coroutine
    def cancel(self, content, article_id, user_id):
        sql = 'insert into `like` value (NULL , %s, %s, %s, %s, %s, %s)'
        now = self.helper.now()
        created_at = now
        updated_at = now
        status = 1
        data = yield self.mysql.execute(sql, content, status, created_at, updated_at, article_id, user_id)
        raise Return(data)

    @coroutine
    def is_like_exist(self, article_id, user_id):
        sql = 'select id from `like` where article_id=%s and user_id=%s limit 1'
        data = yield self.mysql.get(sql, article_id, user_id)
        raise Return(data)


like_model = __LikeModel()
