# coding:utf8
from base import BaseModel
from tornado.gen import coroutine, Return


class __CommentModel(BaseModel):
    @coroutine
    def find_by_id(self, id):
        sql = 'select * from comment where id=%s' % id
        data = yield self.mysql.get(sql)
        raise Return(data)

    @coroutine
    def find_list_by_article_id(self, article_id=0, page=1):
        count = self.count
        sql = 'select * from comment where status=1 and article_id=%s limit %s, %s'
        data = yield self.mysql.query(sql, article_id, count * (page - 1), count)
        raise Return(data)

    @coroutine
    def create(self, content, user_id, article_id):
        sql = 'insert into comment value (NULL , %s, %s, %s, %s, %s, %s)'
        now = self.helper.now()
        created_at = now
        updated_at = now
        status = 1
        data = yield self.mysql.execute(sql, content, status, created_at, updated_at, article_id, user_id)
        raise Return(data)

    @coroutine
    def update(self, id, name, intro):
        sql = 'update comment as t set t.name=%s, t.intro=%s, t.updated_at=%s where id=%s'
        now = self.helper.now()
        updated_at = now
        data = yield self.mysql.execute(sql, name, intro, updated_at, id)
        raise Return(data)


comment_model = __CommentModel()
