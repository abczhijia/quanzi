from database.mysql import mysql
from util import helper


class BaseModel(object):
    def __init__(self):
        self.max_count = 100
        self.min_count = 10
        self.count = 10
        self.mysql = mysql
        self.helper = helper
