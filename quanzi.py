# coding:utf8
import sys, os

reload(sys)
sys.setdefaultencoding("utf-8")
import tornado.web
from tornado.httpserver import HTTPServer
from tornado.ioloop import IOLoop
from tornado.options import options, parse_command_line, define
from tornado.web import StaticFileHandler

define('port', default=7000, type=int)
define('debug', default=True, type=bool)
define('dev', default=True, type=bool)
define('log_file_prefix', default='./log/test.log')
define('log_rotate_mode', default='time')
define('log_rotate_when', default='D')
define('log_rotate_interval', default=1)

parse_command_line()

from handler import index, user, comment, article, like, upload


class Application(tornado.web.Application):
    def __init__(self):
        settings = dict(
            debug=options.debug,
            gzip=True,
            cookie_secret="12345",
            # xsrf_cookies=True,
            template_path=os.path.join(os.path.dirname(__file__), 'front/template'),
            static_path=os.path.join(os.path.dirname(__file__), 'front/static'),
        )

        handlers = [
            (r'/', index.IndexHandler),
            (r'/user/login', user.UserLoginHandler),
            (r'/user/register', user.UserRegisterHandler),
            (r'/upload/image', upload.UploadImageHandler),
            (r'/api/upload/image', upload.ApiUploadImageHandler),
            (r'/api/login', user.ApiUserLoginHandler),
            (r'/api/logout', user.ApiUserLogoutHandler),
            (r'/api/register', user.ApiUserRegisterHandler),
            (r'/api/user/list', user.ApiUserListHandler),
            (r'/api/user/detail/(\d+)', user.ApiUserDetailHandler),
            (r'/api/article/add', article.ApiArticleAddHandler),
            (r'/api/article/list', article.ApiArticleListHandler),
            (r'/api/article/detail/(\d+)', article.ApiArticleDetailHandler),
            (r'/api/comment/add', comment.ApiCommentAddHandler),
            (r'/api/comment/list', comment.ApiCommentListHandler),
            (r'/api/like/add', like.ApiLikeAddHandler),
            (r'/api/download/(.*)', StaticFileHandler, {'path': settings['static_path']})
        ]

        super(Application, self).__init__(handlers=handlers, **settings)


def main():
    http_server = HTTPServer(Application(), xheaders=True)
    http_server.listen(options.port)
    print 'server is listening on http://localhost:%s' % options.port
    IOLoop.instance().start()


if __name__ == '__main__':
    main()
