# coding:utf8
from tornado.gen import coroutine, Return
from .base import BaseService


class _CommentService(BaseService):
    @coroutine
    def find_list_by_article_id(self, article_id, page):
        data = yield self.comment_model.find_list_by_article_id(article_id, page)
        for comment in data:
            comment['user_info'] = yield self.user_model.find_by_id(comment.user_id)
        raise Return(data)


comment_srv = _CommentService()
