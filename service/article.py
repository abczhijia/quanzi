# coding:utf8
from tornado.gen import coroutine, Return
from .base import BaseService


class _ArticleService(BaseService):
    @coroutine
    def find_by_id(self, article_id):
        data = yield self.article_model.find_by_id(article_id)
        user_info = yield self.user_model.find_by_id(data.user_id)
        data['user_info'] = user_info
        raise Return(data)


article_srv = _ArticleService()
