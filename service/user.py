# coding:utf8
from .base import BaseService
from tornado.gen import coroutine, Return


class _UserService(BaseService):
    @coroutine
    def login_by_username_password(self, username, password):
        data = yield self.user_model.find_by_username_password(username, password)
        if data is None:
            raise Return(None)

        data['token'] = yield self.user_model.update_token()
        raise Return(data)

    @coroutine
    def login_by_phone_password(self, phone, password):
        data = yield self.user_model.find_by_phone_password(phone, password)
        if data is None:
            raise Return(None)
        raise Return(data)


user_srv = _UserService()
