# coding:utf8
from tornado.gen import coroutine, Return
from model.user import user_model
from model.article import article_model
from model.comment import comment_model


class BaseService(object):
    def __init__(self):
        self.user_model = user_model
        self.article_model = article_model
        self.comment_model = comment_model
